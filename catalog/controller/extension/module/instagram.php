<?php
class ControllerExtensionModuleInstagram extends Controller {
	public function index() {

		ini_set('xdebug.var_display_max_depth', 5);
		ini_set('xdebug.var_display_max_children', 256);
		ini_set('xdebug.var_display_max_data', 1024);

		$file = DIR_APPLICATION . 'view/javascript/instagram/css/mycustom.css';
		$filename = 'instagram.log';

		$this->load->language('extension/module/instagram');

		$data['heading_title'] = $this->language->get('heading_title');
		
		$this->document->addStyle('catalog/view/javascript/instagram/settings.css');

		$this->document->addStyle('catalog/view/javascript/instagram/slick/slick.css');
		$this->document->addStyle('catalog/view/javascript/instagram/slick/slick-theme.css');

		if( file_exists($file) ){
			$this->document->addStyle('catalog/view/javascript/instagram/css/mycustom.css');
		}else{			
			$this->document->addStyle('catalog/view/javascript/instagram/css/custom.css');
		}

		$this->document->addScript('catalog/view/javascript/instagram/slick/slick.min.js');
		
		$json_link="https://api.instagram.com/v1/users/self/media/recent/?";

		if ($this->request->server['HTTPS']) {
			$json_link.="access_token=".str_replace('http', 'https', $this->config->get('module_instagram_access_token'))."&count={$this->config->get('module_instagram_photo_amount')}";	
		} else {
			$json_link.="access_token=".$this->config->get('module_instagram_access_token')."&count={$this->config->get('module_instagram_photo_amount')}";		
		}

		$data['log'] = '';
		$data['error_warning'] = '';		

		$cUrl = curl_init();

		curl_setopt($cUrl, CURLOPT_URL, $json_link);
		curl_setopt($cUrl, CURLOPT_RETURNTRANSFER, true);

		$returnCurl = curl_exec($cUrl);

		if($returnCurl){
			$json = $returnCurl;
			$instagram = json_decode($json, true);
			$res = array();

			foreach ($instagram['data'] as $instagram ) {				
				array_push($res, array(
					'href'  => $instagram['link'],
					'likes' => $instagram['likes']['count'],
					'comments' => $instagram['comments']['count'],
					'img' 	=> str_replace('http://', 'https://', $instagram['images']['standard_resolution']['url']),
				));
			}

			header('Content-Type: application/json; charset=UTF-8');
			echo json_encode($res);
		} else {
			$this->log = new log($filename);
			$this->log->write(file_get_contents($json_link));
		}
	}
}