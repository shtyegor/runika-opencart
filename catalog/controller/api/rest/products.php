<?php

class ControllerApiRestProducts extends Controller {

	public function index () {
        
        // Image tool
        $this->load->model('tool/image');

        // Products
        $this->load->model('catalog/product');

        $type = isset($_GET['type']) ? $_GET['type'] : NULL;
        $limit = intval($_GET['limit']);
        $start = intval($_GET['start']);

        $productsRes = array();
        
        if(!$type) {
            $category_id = $_GET['category_id'];
            $search_word = isset($_GET['search']) ? $_GET['search'] : NULL;

            $params = array(
                'filter_category_id' => $category_id,
                'filter_name' => $search_word,
                'start' => $start,
                'limit' => $limit
            );

            $productsRes = $this->model_catalog_product->getProducts($params);
        } else if($type === 'latest') {
            $productsRes = $this->model_catalog_product->getLatestProducts($limit);
        } else if($type === 'popular') {
            $productsRes = $this->model_catalog_product->getPopularProducts($limit);
        } else if($type === 'bestseller') {
            $productsRes = $this->model_catalog_product->getBestSellerProducts($limit);
        } else {
            return;
        }

        
        $products = array();

        foreach ($productsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($products, array(
                'product_id' => (int)$product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice,
                'description' => $product['description']
            ));
        }

        // Total
        $total = count($products);
        if(!$type) {
            $total = $this->model_catalog_product->getTotalProducts($params);
        }

        $result = array(
            'products' => $products,
            'total' => (int)$total
        );

		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($result);
    }

}