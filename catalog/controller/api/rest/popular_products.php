<?php

class ControllerApiRestPopularProducts extends Controller {

	public function index () {

        // Image tool
        $this->load->model('tool/image');

        // Popular products
        $this->load->model('catalog/product');

        $popularProductsRes = $this->model_catalog_product->getPopularProducts(4);
        $popularProducts = array();

        foreach ($popularProductsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($popularProducts, array(
                'product_id' => (int)$product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }


        $result = $popularProducts;


        header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($result);
    }

}