<?php

class ControllerApiRestCategories extends Controller {

	public function index () {

        $this->load->model('catalog/category');

        $categoriesRoot = $this->model_catalog_category->getCategories();

        $categories = array();

        foreach ($categoriesRoot as $category) {

            $subcategoriesList = $this->model_catalog_category->getCategories($category['category_id']);
            $subcategories = array();

            foreach ($subcategoriesList as $subcategory) {
                array_push($subcategories, array(
                    'category_id' => $subcategory['category_id'],
                    'name' => $subcategory['name']
                ));
            }

            array_push($categories, array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'top' => (int)$category['top'],
                'subcategories' => $subcategories
            ));
        }


        header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($categories);
    
    }

}