<?php

class ControllerApiRestDownloadPrices extends Controller {
	public function index () {

        $downloads = $this->db->query("SELECT * FROM `oc_download`")->rows;
        $download_descriptions = $this->db->query("SELECT * FROM `oc_download_description`")->rows;

        $prices = array();

        foreach($download_descriptions as $description) {
            $infos = explode("--", $description['name']);
            
            $file = array();

            foreach($downloads as $f) {
                if($f['download_id'] == $description['download_id']) {
                    $file = $f;
                    break;
                }
            }

            if(count($infos) == 3 && $file) {
                if($infos[0] == 'Прайси') {
                    array_push($prices, array(
                        'id' => $file['download_id'],
                        'filename' => $file['mask'],
                        'lang' => $infos[1],
                        'name' => $infos[2]
                    ));
                }
            }
        }

        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($prices);

        
    }
}