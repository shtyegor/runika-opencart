<?php

class ControllerApiRestAccountCart extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token']) || !isset($_POST['type']) || !isset($_POST['product_id'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $token = $_POST['token'];

        if($token === 'anonymous') {
            $customer_id = $_POST['customer_id'];
            if(!$customer_id) {
                $customer_id = rand(100000000, 999999999);
            }
        } else {
            $this->load->model('account/customer');

            $customer = $this->model_account_customer->getCustomerByToken($token);

            if(!$customer) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Відмовлено в доступі'
                ));
                return;
            }

            $customer_id = $customer['customer_id'];
        }

        

        $type = $_POST['type'];
        $product_id = $_POST['product_id'];

        $this->load->model('account/cart');

        if ($type === 'add') {
            $quantity = $_POST['quantity'];

            $cart_items = $this->model_account_cart->getCartItem($customer_id, $product_id);
            $cart_items_len = count($cart_items);

            if($cart_items_len > 0) {
                $this->model_account_cart->editCartQuantity($customer_id, $product_id, $quantity);
            } else {
                $this->model_account_cart->addCart($customer_id, $product_id, $quantity);
            }

            
            echo json_encode(array(
                'anonymous_id' => $token === 'anonymous' ? $customer_id : false,
                'status' => true
            ));
            return;
        }
        if($type === 'delete') {
            $this->model_account_cart->deleteCart($customer_id, $product_id);
            echo json_encode(array(
                'status' => true
            ));
            return;
        }
        if ($type === 'clear') {
            $this->model_account_cart->clearCart($customer_id);
            echo json_encode(array(
                'status' => true
            ));
            return;
        }

        echo json_encode(array(
            'status' => false
        ));
    }
}