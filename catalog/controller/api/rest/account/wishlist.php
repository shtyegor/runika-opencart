<?php

class ControllerApiRestAccountWishlist extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token']) || !isset($_POST['type']) || !isset($_POST['product_id'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $token = $_POST['token'];

        $this->load->model('account/customer');

        $customer = $this->model_account_customer->getCustomerByToken($token);

        if(!$customer) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Відмовлено в доступі'
            ));
            return;
        }

        $type = $_POST['type'];
        $product_id = $_POST['product_id'];

        $this->load->model('account/wishlist');

        if($type === 'delete') {
            $this->model_account_wishlist->deleteWishlistByCustomer($customer['customer_id'], $product_id);
            echo json_encode(array(
                'status' => true
            ));
            return;
        }
        if ($type === 'add') {
            $this->model_account_wishlist->addWishlistByCustomer($customer['customer_id'], $product_id);
            echo json_encode(array(
                'status' => true
            ));
            return;
        }

        echo json_encode(array(
            'status' => false
        ));
    }
}