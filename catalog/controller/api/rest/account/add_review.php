<?php

class ControllerApiRestAccountAddReview extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $token = $_POST['token'];

        $this->load->model('account/customer');

        $customer = $this->model_account_customer->getCustomerByToken($token);

        if(!$customer) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Відмовлено в доступі'
            ));
            return;
        }

        if(!isset($_POST['product_id']) || !isset($_POST['review_text'])) {
            return;
        }

        $product_id = $_POST['product_id'];
        $review_text = $_POST['review_text'];

        $this->load->model('catalog/review');

        $review_data = array(
            'name' => $customer['firstname'] . ' ' . $customer['lastname'],
            'customer_id' => $customer['customer_id'],
            'product_id' => $product_id,
            'text' => $review_text,
            'rating' => 5
        );

        $this->model_catalog_review->addReview($review_data);

        echo json_encode(array(
            'status' => true
        ));
    }
}