<?php

class ControllerApiRestAccountEdit extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token']) || !isset($_POST['type'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $ip = $_SERVER['REMOTE_ADDR'];
        $token = $_POST['token'];

        $this->load->model('account/customer');

        $customer = $this->model_account_customer->getCustomerByToken($token);

        if(!$customer) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Відмовлено в доступі'
            ));
            return;
        }

        $type = $_POST['type'];

        if($type === 'password') {
            if(!isset($_POST['password'])) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Новий пароль не надіслано'
                ));
                return;
            }
            $password = $_POST['password'];

            $editPassword = $this->model_account_customer->editPassword($customer['email'], $password);

            echo json_encode(array(
                'status' => true
            ));
            return;
        }

        if($type === 'newsletter') {
            if(!isset($_POST['newsletter'])) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Не надіслана згода на підписку'
                ));
                return;
            }
            $newsletter = $_POST['newsletter'];

            $editNewsletter = $this->model_account_customer->editNewsletterByCustomer($customer['customer_id'], $newsletter);

            echo json_encode(array(
                'status' => true
            ));
            return;
        }

        if($type === 'userinfo') {
            if(!isset($_POST['firstname']) || !isset($_POST['lastname']) || !isset($_POST['email']) || !isset($_POST['telephone']) || !isset($_POST['area']) || !isset($_POST['city']) || !isset($_POST['address_home']) || !isset($_POST['address_np'])) {
                echo json_encode(array(
                    'status' => false,
                    'message' => 'Не коректно надіслані дані користувача'
                ));
                return;
            }
            
            $user_data = array(
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email'],
                'telephone' => $_POST['telephone']
            );
            $this->model_account_customer->editCustomer($customer['customer_id'], $user_data);


            $this->load->model('account/address');

            $address_data = array(
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'company' => '',
                'address_1' => $_POST['address_home'],
                'address_2' => $_POST['address_np'],
                'postcode' => $_POST['area'],
                'city' => $_POST['city'],
                'zone_id' => 3502,
                'country_id' => 220
            );
            
            if((int)$customer['address_id'] === 0) {
                $address_id = $this->model_account_address->addAddress($customer['customer_id'], $address_data);
                $this->model_account_customer->editAddressId($customer['customer_id'], $address_id);
            } else {
                $this->model_account_address->editAddress($customer['address_id'], $address_data);
            }

            echo json_encode(array(
                'status' => true,
                'address_id' => $customer['address_id'],
                'address' => $this->model_account_address->getAddress($customer['address_id'])
            ));
            return;
        }

        echo json_encode(array(
            'status' => false,
            'message' => 'Щось пішло не так'
        ));

    }
}