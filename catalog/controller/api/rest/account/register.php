<?php

class ControllerApiRestAccountRegister extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['firstname']) || !isset($_POST['lastname']) || !isset($_POST['email']) || !isset($_POST['telephone']) || !isset($_POST['password'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $telephone = $_POST['telephone'];
        $password = $_POST['password'];


        $this->load->model('account/customer');

        $is_busy_email = $this->model_account_customer->getCustomerByEmail($email);

        if($is_busy_email) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Вже існує користувач з такою електронною поштою'
            ));
            return;
        }

        

        $add_customer_id = $this->model_account_customer->addCustomer(array(
            'customer_group_id' => 1,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'telephone' => $telephone,
            'fax' => '',
            'password' => $password,
            'newsletter' => 1
        ));

        $customer = $this->model_account_customer->getCustomer($add_customer_id);


        $token = md5(rand());
        $this->model_account_customer->editToken($customer['customer_id'], $token);


        $this->load->model('account/address');
        $address = $this->model_account_address->getAddressByCustomer($customer['customer_id'], $customer['address_id']);


        $this->load->model('tool/image');
        $this->load->model('catalog/product');


        $this->load->model('account/wishlist');

        $wishlist = $this->model_account_wishlist->getWishlistByCustomer($customer['customer_id']);
        $wishlist_total = count($wishlist);

        $wishlist_products = array();
        foreach ($wishlist as $wishlist_item) {
            $product = $this->model_catalog_product->getProduct($wishlist_item['product_id']);
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($wishlist_item['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($wishlist_products, array(
                'product_id' => (int)$product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }


        $this->load->model('account/cart');

        $cart = $this->model_account_cart->getCartByCustomer($customer['customer_id']);
        $cart_total = count($cart);

        $cart_products = array();
        foreach ($cart as $cart_item) {
            $product = $this->model_catalog_product->getProduct($cart_item['product_id']);
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($cart_item['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($cart_products, array(
                'product_id' => (int)$product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice,
                'quantity' => (int)$cart_item['quantity']
            ));
        }
        

        echo json_encode(array(
            'status' => true,
            'token' => $token,
            'user' => array(
                'firstname' => $customer['firstname'],
                'lastname' => $customer['lastname'],
                'email' => $customer['email'],
                'telephone' => $customer['telephone'],
                'newsletter' => (int)$customer['newsletter'],
                'address_home' => $address['address_home'],
				'address_np' => $address['address_np'],
				'area' => $address['area'],
				'city' => $address['city']
            ),
            'cart' => array(
                'total' => (int)$cart_total,
                'products' => $cart_products
            ),
            'wishlist' => array(
                'total' => (int)$wishlist_total,
                'products' => $wishlist_products
            )
        ));
    }
}