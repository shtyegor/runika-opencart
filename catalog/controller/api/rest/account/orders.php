<?php

class ControllerApiRestAccountOrders extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $token = $_POST['token'];

        $this->load->model('account/customer');

        $customer = $this->model_account_customer->getCustomerByToken($token);

        if(!$customer) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Відмовлено в доступі'
            ));
            return;
        }

        $start = $_POST['start'];

        $this->load->model('account/order');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $orders = array();
        $ordersRes = $this->model_account_order->getOrders($customer['customer_id'], $start, 20);

        foreach ($ordersRes as $order) {
            $order_products = array();
            $productsRes = $this->model_account_order->getOrderProducts($order['order_id']);

            foreach ($productsRes as $product) {
                $fullProduct = $this->model_catalog_product->getProduct($product['product_id']);
                
                array_push($order_products, array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'image' => $this->model_tool_image->resize($fullProduct['image'], 192, 292),
                    'price' => (int)$product['price'],
                    'total' => (int)$product['total'],
                    'quantity' => (int)$product['quantity']
                ));
            }

            array_push($orders, array(
                'order_id' => $order['order_id'],
                'firstname' => $order['firstname'],
                'lastname' => $order['lastname'],
                'status' => $order['status'],
                'date_added' => $order['date_added'],
                'total' => (int)$order['total'],
                'products' => $order_products
            ));
        }

        echo json_encode($orders);
    }
}