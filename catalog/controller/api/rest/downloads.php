<?php

class ControllerApiRestDownloads extends Controller {
	public function index () {

        $type = $this->request->post['type'];

        $downloads = $this->db->query("SELECT * FROM `oc_download`")->rows;
        $download_descriptions = $this->db->query("SELECT * FROM `oc_download_description`")->rows;

        $prices = array();
        $groups = array();

        foreach($download_descriptions as $description) {
            $infos = explode("--", $description['name']);
            
            $file = array();

            foreach($downloads as $f) {
                if($f['download_id'] == $description['download_id']) {
                    $file = $f;
                    break;
                }
            }

            if(count($infos) == 3 && $file) {
                if($infos[0] == 'Прайси') {
                    array_push($prices, array(
                        'id' => $file['download_id'],
                        'filename' => $file['mask'],
                        'lang' => $infos[1],
                        'name' => $infos[2]
                    ));
                } else if($infos[0] == 'Група товарів') {
                    array_push($groups, array(
                        'id' => $file['download_id'],
                        'filename' => $file['mask'],
                        'title' => $infos[1],
                        'group_id' => $infos[2]
                    ));
                }
            }
        }

        header('Content-Type: application/json; charset=UTF-8');
        if($type == 'prices') {
            echo json_encode($prices);
        } else if($type == 'groups') {
            echo json_encode($groups);
        }
        
    }
}