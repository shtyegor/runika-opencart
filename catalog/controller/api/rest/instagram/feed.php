<?php

class ControllerApiRestInstagramFeed extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        $instagram = new \InstagramScraper\Instagram();
        $nonPrivateAccountMedias = $instagram->getMedias('runika.book');

        $feed = array();

        for ($i = 0; $i < 6; $i++) {
            array_push($feed, array(
                'image' => $nonPrivateAccountMedias[$i] -> getImageThumbnailUrl(),
                'link' => $nonPrivateAccountMedias[$i] -> getLink(),
                'likesCount' => $nonPrivateAccountMedias[$i] -> getLikesCount(),
                'commentsCount' => $nonPrivateAccountMedias[$i] -> getCommentsCount(),
            ));
        }
        echo json_encode($feed);
    }
}