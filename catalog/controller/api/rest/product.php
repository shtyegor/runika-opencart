<?php

class ControllerApiRestProduct extends Controller {

	public function index () {
        $product_id = $_GET['product_id'];

        // Image tool
        $this->load->model('tool/image');

        // Product
        $this->load->model('catalog/product');
        $this->load->model('catalog/review');
        $this->load->model('catalog/category');

        $productRes = $this->model_catalog_product->getProduct($product_id);

        $productDiscountsRes = $this->model_catalog_product->getProductDiscounts($product_id);
        $productPrice = $productRes['price'];
        $productOldPrice = NULL;
        if(count($productDiscountsRes) > 0) {
            $productOldPrice = $productRes['price'];
            $productPrice = $productDiscountsRes[0]['price'];
        }

        $productImagesRes = $this->model_catalog_product->getProductImages($product_id);
        $productImages = array();
        array_push($productImages, $this->model_tool_image->resize($productRes['image'], 340, 565));
        foreach ($productImagesRes as $images) {
            array_push($productImages, $this->model_tool_image->resize($images['image'], 340, 565));
        }

        $reviewsTotal = $this->model_catalog_review->getTotalReviewsByProductId($product_id);
        $reviews = array();
        if($reviewsTotal > 0) {
            $reviewsRes = $this->model_catalog_review->getReviewsByProductId($product_id);
            foreach ($reviewsRes as $review) {
                array_push($reviews, array(
                    'author' => $review['author'],
                    'text' => $review['text']
                ));
            }
        }

        $categoriesRes = $this->model_catalog_product->getCategories($product_id);
        $categoriesList = array();
        $categoryGroupId = '79';
        $categoryGroups = $this->model_catalog_category->getCategories($categoryGroupId);
        $categoryGroupsIdList = array();
        foreach ($categoryGroups as $group) {
            array_push($categoryGroupsIdList, $group['category_id']);
        }

        $levels = array();
        $activeLevel = NULL;
        foreach ($categoriesRes as $c) {
            $category = $this->model_catalog_category->getCategory($c['category_id']);
            if(in_array($category['parent_id'], $categoryGroupsIdList)) {
                $levelsRes = $this->model_catalog_category->getCategories($category['parent_id']);
                foreach ($levelsRes as $level) {
                    $isActive = false;
                    if($category['name'] == $level['name']) {
                        $isActive = true;
                    }
                    array_push($levels, array(
                        'name' => $level['name'],
                        'group_id' => $level['category_id'],
                        'is_active' => $isActive
                    ));
                }
            }
        }

        $productResult = array(
            'product_id' => (int)$productRes['product_id'],
            'name' => $productRes['name'],
            'description' => $productRes['description'],
            'price' => (int)$productPrice,
            'old_price' => (int)$productOldPrice,
            'image' => $this->model_tool_image->resize($productRes['image'], 192, 292),
            'images' => $productImages,
            'author' => $productRes['model'],
            'sku' => $productRes['sku'],
            'year' => (int)$productRes['upc'],
            'pages' => $productRes['ean'],
            'cover' => $productRes['jan'],
            'isbn' => $productRes['isbn'],
            'code' => $productRes['mpn'],
            'manufacturer' => $productRes['manufacturer'],
            'reviews_total' => $reviewsTotal,
            'reviews' => $reviews,
            'levels' => $levels,
            'seo' => array(
                'title' => $productRes['meta_title'],
                'description' => $productRes['meta_description'],
                'keywords' => $productRes['meta_keyword']
            )
        );

        // Related products
        $productsRelatedRes = $this->model_catalog_product->getProductRelated($product_id);
        $productsRelated = array();
        foreach ($productsRelatedRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($productsRelated, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }

        // Popular products
        $popularProductsRes = $this->model_catalog_product->getPopularProducts(3);
        $popularProducts = array();

        foreach ($popularProductsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($popularProducts, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }


        $result = array(
            'product' => $productResult,
            'related' => $productsRelated,
            'popular' => $popularProducts
        );


        header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($result);
    }

}