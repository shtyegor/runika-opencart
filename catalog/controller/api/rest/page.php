<?php

class ControllerApiRestPage extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        $page_id = $_GET['page_id'];

        $this->load->model('catalog/information');

        $information = $this->model_catalog_information->getInformation($page_id);

        
		echo json_encode(array(
            'title' => $information['title'],
            'description' => $information['description'],
            'meta_title' => $information['meta_title'],
            'meta_description' => $information['meta_description'],
            'meta_keyword' => $information['meta_keyword'],
        ));
    }

}