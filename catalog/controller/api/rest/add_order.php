<?php

class ControllerApiRestAccountAddOrder extends Controller {

	public function index () {
        header('Content-Type: application/json; charset=UTF-8');

        if($_SERVER['REQUEST_METHOD'] != 'POST') {
            return;
        }

        if(!isset($_POST['token'])) {
            echo json_encode(array(
                'status' => false,
                'message' => 'Надіслано не коректні дані'
            ));
            return;
        }

        $data = array(
            'invoice_prefix'          => 'INV-2019-00',
            'store_id'                => 0,
            'store_name'              => 'Книжковий магазин Runika',
            'store_url'               => 'https://runika.com.ua/',
            'customer_id'             => '',
            'customer_group_id'       => 1,
            'firstname'               => '',
            'lastname'                => '',
            'telephone'               => '',
            'payment_firstname'       => '',
            'payment_lastname'        => '',
            'payment_company'         => '',
            'payment_address_1'       => '',
            'payment_address_2'       => '',
            'payment_postcode'        => '',
            'payment_city'            => '',
            'payment_zone_id'         => '',
            'payment_zone'            => '',
            'payment_zone_code'       => '',
            'payment_country_id'      => '',
            'payment_country'         => '',
            'payment_iso_code_2'      => '',
            'payment_iso_code_3'      => '',
            'payment_address_format'  => '',
            'payment_method'          => '',
            'shipping_firstname'      => '',
            'shipping_lastname'       => '',
            'shipping_company'        => '',
            'shipping_address_1'      => '',
            'shipping_address_2'      => '',
            'shipping_postcode'       => '',
            'shipping_city'           => '',
            'shipping_zone_id'        => 3501,
            'shipping_zone'           => '',
            'shipping_zone_code'      => '',
            'shipping_country_id'     => 220,
            'shipping_country'        => '',
            'shipping_iso_code_2'     => '',
            'shipping_iso_code_3'     => '',
            'shipping_address_format' => '',
            'shipping_method'         => '',
            'comment'                 => '',
            'total'                   => '',
            'language_id'             => 2,
            'currency_id'             => 4,
            'currency_code'           => 'UAH',
            'currency_value'          => 1,
            'marketing_id'            => 0,
            'affiliate_id'            => '',
            'tracking'                => '',
            'forwarded_ip'            => '',
            'ip'                      => $_SERVER['REMOTE_ADDR'],
            'user_agent'              => $_SERVER['HTTP_USER_AGENT'],
            'accept_language'         => 'uk'
        );

        $this->load->model('checkout/order');

        $this->model_checkout_order->addOrder($data);

    }
}