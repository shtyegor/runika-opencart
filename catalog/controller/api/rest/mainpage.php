<?php

class ControllerApiRestMainpage extends Controller {

	public function index () {

        // Image tool
        $this->load->model('tool/image');

        // Banners
        $this->load->model('design/banner');

        $bannersRes = $this->model_design_banner->getBanner(7);

        $banners = array();

        foreach ($bannersRes as $banner) {
            array_push($banners, array(
                'title' => $banner['title'],
                'link'  => $banner['link'],
                'image' => $this->model_tool_image->resize($banner['image'], 1405, 370)
            ));
        }

        
        // Products
        $this->load->model('catalog/product');

        // Popular products
        $popularProductsRes = $this->model_catalog_product->getPopularProducts(5);
        $popularProducts = array();

        foreach ($popularProductsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($popularProducts, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }

        // Latest products
        $latestProductsRes = $this->model_catalog_product->getLatestProducts(2);
        $latestProducts = array();

        foreach ($latestProductsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($latestProducts, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice,
                'description' => $product['description']
            ));
        }

        // BestSeller products
        $bestSellerProductsRes = $this->model_catalog_product->getBestSellerProducts(5);
        $bestSellerProducts = array();

        foreach ($bestSellerProductsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($bestSellerProducts, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => (int)$pPrice,
                'old_price' => (int)$pOldPrice
            ));
        }


        // Information
        $this->load->model('catalog/information');

        $informationRes = $this->model_catalog_information->getInformation(4);
        $information = array(
            'title' => $informationRes['title'],
            'description' => $informationRes['description'],
        );


        $result = array(
            'banners' => $banners,
            'products' => array(
                'popular' => $popularProducts,
                'latest' => $latestProducts,
                'bestseller' => $bestSellerProducts
            ),
            'information' => $information
        );

		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($result);
	}
}