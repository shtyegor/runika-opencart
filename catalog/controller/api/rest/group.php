<?php

class ControllerApiRestGroup extends Controller {

	public function index () {
        $group_id = $_GET['group_id'];

        // Image tool
        $this->load->model('tool/image');

        // Products
        $this->load->model('catalog/product');

        $productsRes = $this->model_catalog_product->getProducts(array(
            'filter_category_id' => $group_id
        ));
        $products = array();

        foreach ($productsRes as $product) {
            $pDiscountsRes = $this->model_catalog_product->getProductDiscounts($product['product_id']);
            $pPrice = $product['price'];
            $pOldPrice = NULL;
            if(count($pDiscountsRes) > 0) {
                $pOldPrice = $product['price'];
                $pPrice = $pDiscountsRes[0]['price'];
            }
            array_push($products, array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'image' => $this->model_tool_image->resize($product['image'], 192, 292),
                'price' => $pPrice,
                'old_price' => $pOldPrice,
                'description' => $product['description']
            ));
        }

        // Levels
        $this->load->model('catalog/category');

        $levels = array();
        $activeLevel = NULL;
        $category = $this->model_catalog_category->getCategory($group_id);
        $levelsRes = $this->model_catalog_category->getCategories($category['parent_id']);
        foreach ($levelsRes as $level) {
            $isActive = false;
            if($category['name'] == $level['name']) {
                $isActive = true;
            }
            array_push($levels, array(
                'name' => $level['name'],
                'group_id' => $level['category_id'],
                'is_active' => $isActive
            ));
        }

        $result = array(
            'products' => $products,
            'levels' => $levels
        );

		header('Content-Type: application/json; charset=UTF-8');
		echo json_encode($result);
    }
}