
<?php

require './src/Delivery/NovaPoshtaApi2.php';
use LisDev\Delivery\NovaPoshtaApi2;

$NOVA_POSHTA_KEY = 'eefb22b6444c3d3d4ecfe8dc59804d78';

$np = new NovaPoshtaApi2(
    $NOVA_POSHTA_KEY,
    'ua',
    TRUE,
    'curl'
);

function createDir($path) {
    if(!is_dir($path)) {
        mkdir($path, 0700);
    }
}

function writeFile($path, $text) {
    $fp = fopen($path, 'w');
    fwrite($fp, '');
    fwrite($fp, $text);
    fclose($fp);
}

$country = array();
$areas = array();
$cities = array();

createDir('./json');

$areas_data = $np -> getAreas();
foreach($areas_data['data'] as $area) {
    if($area['Ref'] != '71508128-9b87-11de-822f-000c2965ae0e') {
        createDir('./json/'.$area['Ref']);

        array_push($country, array(
            'Name' => $area['Description'],
            'Ref' => $area['Ref']
        ));
        $areas[$area['Ref']] = array();
    }
}

$cities_data = $np -> getCities();
foreach($cities_data['data'] as $city) {
    createDir('./json/'.$city['Area'].'/'.$city['Ref']);

    $new_city_arr = array(
        'Name' => $city['Description'],
        'Ref' => $city['Ref'],
        'Area' => $city['Area'],
        'Warehouses' => array()
    );
    $cities[$city['Ref']] = $new_city_arr;

    $new_areas_city_arr = array(
        'Name' => $city['Description'],
        'Ref' => $city['Ref']
    );
    array_push($areas[$city['Area']], $new_areas_city_arr);
}

$warehouses_data = $np -> getWarehousesAll();
foreach($warehouses_data['data'] as $warehouse) {
    $new_warehouse_arr = array(
        'Number' => $warehouse['Number'],
        'Address' => explode(': ', $warehouse['Description'])[1]
    );
    if(strlen($cities[$warehouse['CityRef']]['Ref'] > 0)) {
        array_push($cities[$warehouse['CityRef']]['Warehouses'], $new_warehouse_arr);
    }
}

writeFile('./json/areas.json', json_encode($country));

foreach($country as $area) {
    writeFile('./json/'.$area['Ref'].'/cities.json', json_encode($areas[$area['Ref']]));
}

foreach($cities as $city) {
    writeFile('./json/'.$city['Area'].'/'.$city['Ref'].'/warehouses.json', json_encode($city['Warehouses']));
}