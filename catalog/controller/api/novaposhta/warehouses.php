<?php

class ControllerApiNovaposhtaWarehouses extends Controller {
    public function index () {
        $dir = getcwd();
        $area_id = $this->request->post['area_id'];
        $city_id = $this->request->post['city_id'];

        if($area_id) {
            $json = file_get_contents($dir . '/catalog/controller/api/novaposhta/json/' . $area_id . '/' . $city_id . '/warehouses.json');

            header('Content-type: application/json');
            echo $json;
        }
    }
}