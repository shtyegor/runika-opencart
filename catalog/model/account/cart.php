<?php
class ModelAccountCart extends Model {
    public function getCartByCustomer($customer_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "'");
		
		return $query->rows;
    }

    public function getCartItem($customer_id, $product_id) {
        $query = $this->db->query(
            "SELECT * FROM " . DB_PREFIX . "cart WHERE " .
            "customer_id = '" . (int)$customer_id . "' " .
            "AND " .
            "product_id = '" . (int)$product_id . "'"
        );
		
		return $query->rows;
    }
    
    public function addCart($customer_id, $product_id, $quantity) {
        $this->db->query(
            "INSERT INTO " . DB_PREFIX . "cart SET " .
            "api_id = '0', " .
            "customer_id = '" . (int)$customer_id . "', " .
            "session_id = '0', " .
            "product_id = '" . (int)$product_id . "', " .
            "recurring_id = '0', " .
            "quantity = '" . (int)$quantity . "', " .
            "date_added = NOW()"
        );
	}

	public function deleteCart($customer_id, $product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$customer_id . "' AND product_id = '" . (int)$product_id . "'");
    }
    
    public function editCartQuantity($customer_id, $product_id, $quantity) {
        $this->db->query(
            "UPDATE " . DB_PREFIX . "cart SET " .
            "quantity = '" . (int)$quantity . "', " .
            "date_added = NOW() " .
            "WHERE " .
            "customer_id = '" . (int)$customer_id . "' " .
            "AND " .
            "product_id = '" . (int)$product_id . "'"
        );
	}
}