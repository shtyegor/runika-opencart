<?php
// HTTP
define('HTTP_SERVER', 'http://admin.runika.com.ua/');
define('HTTP_CATALOG', 'http://api.runika.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://admin.runika.com.ua/');
define('HTTPS_CATALOG', 'https://api.runika.com.ua/');

// DIR
$DIR_ROOT = '/var/www/runika-opencart/';

define('DIR_APPLICATION', $DIR_ROOT . 'admin/');
define('DIR_SYSTEM', $DIR_ROOT . 'system/');
define('DIR_STORAGE', '/var/www/runika-storage/');
define('DIR_CATALOG', $DIR_ROOT . 'catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_IMAGE', $DIR_ROOT . 'image/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'runikadb.c6sswwopsyur.eu-central-1.rds.amazonaws.com');
define('DB_USERNAME', 'admin');
define('DB_PASSWORD', 'KE85iDYK8JKYy1SBayew');
define('DB_DATABASE', 'runika_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
